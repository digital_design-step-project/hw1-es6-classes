class Employee {
    constructor(info) {
        this._name = info.name;
        this._age = info.age;
        this._salary = info.salary;
    }

    // name
    get name() {
        return this._name;
    }
    set name(newName) {
        this._name = newName;
    }

    // age
    get age() {
        return this._age;
    }
    set age(newAge) {
        this._age = newAge;
    }

    // salary
    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(info, lang) {
        super(info);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const prog1 = new Programmer({
    name: "Zhenia",
    age: 19,
    salary: 850,
}, ["ua", "en"]);

const prog2 = new Programmer({
    name: "Zhora",
    age: 21,
    salary: 1050,
}, ["ua", "en", "pl", "fr"]);

console.log(prog1);
console.log(prog2.lang);